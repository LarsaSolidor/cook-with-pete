import React, { Component } from "react";
import Blurb from "./Blurb";
import ControlledCarousel from "./ControlledCarousel";
import Hero from "./Hero";
import bread_hero from "../images/bread_hero.jpg";

class Home extends Component {
	// state = {  }
	componentDidMount() {
		document.title = "Breaking Bread";
	}

	componentDidUpdate() {
		document.title = "Breaking Bread";
	}

	render() {
		return (
			<>
				<Hero
					img={bread_hero}
					h1_text="Cook with Pete"
					content_text={<p>From Apple Pie to Zwieback.</p>}
					link_to="/classes"
				/>
				<Blurb />
				{/* <ControlledCarousel /> */}
			</>
		);
	}
}

export default Home;
