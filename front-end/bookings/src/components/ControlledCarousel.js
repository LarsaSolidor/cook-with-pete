import React from 'react';
import { Container, Carousel } from 'react-bootstrap'
import breadImg1 from '../images/bread1.JPG';
import breadImg2 from '../images/bread2.jpg';
import breadImg3 from '../images/bread3.jpg';

function ControlledCarousel() {
    const imageSettings = { height: "30em", width: "auto" }

    return (
        <Container >
            <Carousel>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={breadImg1}
                        alt="Oooo Look! Some bread."
                        style={imageSettings}
                    />
                    <Carousel.Caption>
                        <h3>Oooo Look! Some bread</h3>
                        <p>Learn some stuff</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={breadImg2}
                        alt="Third slide"
                        style={imageSettings}
                    />

                    <Carousel.Caption>
                        <h3>Yep, definitely looks like bread.</h3>
                        <p>Make this!</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={breadImg3}
                        alt="Third slide"
                        style={imageSettings}
                    />

                    <Carousel.Caption>
                        <h3>Bready!</h3>
                        <p>Yes</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel></Container>
    )
}

export default ControlledCarousel;