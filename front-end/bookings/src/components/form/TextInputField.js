import React from "react";
import { Container, Button, Form, Col } from "react-bootstrap";
import { Formik, useField } from "formik";
import * as Yup from "yup";

const TextInputField = ({ label, ...props }) => {
	const [field, meta] = useField(props);
	return (
		<>
			{label ? <Form.Label>{label}</Form.Label> : null}
			<Form.Control
				{...field}
				{...props}
				isInvalid={meta.touched && !!meta.error}
				isValid={meta.touched && !meta.error}
			/>
			<Form.Control.Feedback type="invalid">{meta.error}</Form.Control.Feedback>
			<Form.Control.Feedback type="valid">Looks good!</Form.Control.Feedback>
		</>
	);
};

export default TextInputField;
