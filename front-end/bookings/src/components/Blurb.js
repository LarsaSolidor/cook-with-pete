import React, { useState, useEffect } from "react";
import axios from "axios";
import founder_img from "../images/founder.jpg";
import { Container, Image } from "react-bootstrap";

const Blurb = () => {
	const [blurbText, setBlurbText] = useState("");

	useEffect(() => {
		axios
			.get(
				`https://cms.cookwithpete.co.uk/cookwithpete/items/about_us/1?fields=about_us`
			)
			.then((res) => {
				const text = res.data.data.about_us;
				setBlurbText(text);
			});
		// setBlurbText("Some text!");
	});
	return (
		<Container>
			<div className="row">
				<div className="col-md">
					<Image
						className="responsive"
						src={founder_img}
						style={{
							display: "block",
							marginLeft: "auto",
							marginRight: "auto",
							width: "100%",
						}}
						thumbnail
					/>
				</div>
				<div className="col-md">
					<quote>
						{blurbText.split("\n").map((paragraph) => {
							return (
								<span>
									{paragraph}
									<br />
								</span>
							);
						})}
					</quote>
				</div>
			</div>
		</Container>
	);
};

export default Blurb;
