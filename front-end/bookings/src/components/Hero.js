import React from "react";
import { Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const Hero = ({ img, h1_text, content_text, link_to }) => {
	return (
		<div className="hero-section" style={{ backgroundImage: `url("${img}")` }}>
			<div className="hero-section-content">
				<h1>{h1_text}</h1>
				{content_text}
				{link_to ? (
					<LinkContainer to="/classes">
						<Button className="btn btn-primary">Find a class</Button>
					</LinkContainer>
				) : null}
			</div>
		</div>
	);
};

export default Hero;
