import React from "react";
import { Container, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

function EventCard(props) {
	return (
		<Container
			style={{
				boxShadow: "0px 2px 10px 1px rgba(0,0,0,0.4)",
				borderRadius: "15px",
				padding: "2em",
				marginBottom: "1em",
			}}
		>
			<div className="row">
				<div className="col-md-4">
					{props.classInfo.location.photos ? (
						<img
							src={
								props.classInfo.location.photos[0].directus_files_id.data
									.full_url
							}
							alt="venue"
							width="100%"
						/>
					) : null}
				</div>
				<div className="col-md-8">
					<h2>{props.classInfo.start_time}</h2>
					<h4>{props.classInfo.location.venue_name}</h4>
					<h5>
						{props.classInfo.duration === "1"
							? `${props.classInfo.duration} hour `
							: `${props.classInfo.duration} hours `}
						expert tuition
					</h5>
					<div className="row">
						<div className="col-md-8">
							<div>{props.classInfo.event_blurb}</div>
						</div>

						<div
							className="col-md-4"
							style={{ display: "flex", flexDirection: "column" }}
						>
							<div className="class-booking-info">
								{props.classInfo.spaces === "1"
									? `${props.classInfo.spaces} space `
									: `${props.classInfo.spaces} spaces `}
								available
							</div>
							<div className="class-booking-info">
								£{props.classInfo.price_per_person} per person
							</div>
							<Link to={`/classes/${props.classInfo.id}`}>
								<Button className="btn btn-primary btn-block class-booking">
									Book
								</Button>
							</Link>
						</div>
					</div>
				</div>
			</div>
		</Container>
	);
}

export default EventCard;
