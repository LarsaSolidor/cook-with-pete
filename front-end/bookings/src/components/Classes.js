import React, { useState, useEffect } from "react";
import axios from "axios";
import EventCard from "./EventCard";
import { Container } from "react-bootstrap";
import Hero from "./Hero";
import classes_hero from "../images/classes_hero.jpg";
import {
	FcDecision,
	FcCalendar,
	FcMoneyTransfer,
	FcCheckmark,
} from "react-icons/fc";

const Classes = () => {
	// const initialClassesState = [
	// 	{
	// 		id: 1,
	// 		date: "Monday 20th April 2020",
	// 		time: "20:00",
	// 		durationInHours: "1",
	// 		location: "The Bakehouse, Haslington, SG3 5HB",
	// 		spacesAvailable: "1",
	// 		pricePerPerson: "£100.00",
	// 		description:
	// 			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod lectus a dolor interdum facilisis. Ut a malesuada metus, a feugiat sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce id nibh ligula. Fusce elementum elit eros, vel malesuada quam interdum sed. Suspendisse varius placerat enim non tempor.",
	// 	},
	// 	{
	// 		id: 2,
	// 		date: "Monday 20th April 2020",
	// 		time: "22:00",
	// 		durationInHours: "2",
	// 		location: "The Bakehouse, Haslington, SG3 5HB",
	// 		spacesAvailable: "3",
	// 		pricePerPerson: "£75.00",
	// 		description:
	// 			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod lectus a dolor interdum facilisis. Ut a malesuada metus, a feugiat sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce id nibh ligula. Fusce elementum elit eros, vel malesuada quam interdum sed. Suspendisse varius placerat enim non tempor.",
	// 	},
	// 	{
	// 		id: 3,
	// 		date: "Saturday 26th April 2020",
	// 		time: "20:00",
	// 		durationInHours: "2",
	// 		location: "The Abode, Haslington, SG4 5HF",
	// 		spacesAvailable: "8",
	// 		pricePerPerson: "£150.00",
	// 		description:
	// 			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod lectus a dolor interdum facilisis. Ut a malesuada metus, a feugiat sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce id nibh ligula. Fusce elementum elit eros, vel malesuada quam interdum sed. Suspendisse varius placerat enim non tempor.",
	// 	},
	// ];

	const [classes, setClasses] = useState("");

	useEffect(() => {
		window.scrollTo(0, 0);
		document.title = "Classes";
	}, []);

	useEffect(() => {
		axios
			.get(
				`https://cms.cookwithpete.co.uk/cookwithpete/items/events
				?fields=id,spaces,number_booked,price_per_person,event_blurb,
				start_time,duration,location.venue_name,location.city,
				location.photos.directus_files_id.data.full_url
				&?filter[start_time][gte]=now
				&?sort=start_time`
			)
			.then((res) => {
				setClasses(res.data.data);
			});
	});

	return (
		<>
			<Hero
				img={classes_hero}
				h1_text="Our classes"
				content_text={
					<ul style={{ listStyle: "none", padding: "0" }}>
						<li>
							<FcCalendar />
							Choose a class at a time convenient to you
						</li>
						<li>
							<FcDecision />
							Let us know who you are and how many of you are coming
						</li>
						<li>
							<FcMoneyTransfer />
							Make payment
						</li>
						<li>
							<FcCheckmark />
							Come to the class!
						</li>
					</ul>
				}
			/>
			<Container>
				{classes ? (
					classes.map((classInfo) => <EventCard classInfo={classInfo} />)
				) : (
					<div>Loading classes...</div>
				)}
			</Container>
		</>
	);
};

export default Classes;
