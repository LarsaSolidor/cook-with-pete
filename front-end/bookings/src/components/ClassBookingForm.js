import React, { useState, useEffect } from "react";
import { Container, Button, Form, Col } from "react-bootstrap";
import moment from "moment";
import { Formik, getFieldProps } from "formik";
import * as Yup from "yup";
import TextInputField from "./form/TextInputField";

const ClassBookingForm = () => {
	const initialClassState = {
		id: 1,
		date: "Monday 20th April 2020",
		time: "20:00",
		durationInHours: "1",
		location: "The Bakehouse, Haslington, SG3 5HB",
		spacesAvailable: "8",
		pricePerPerson: "£100.00",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod lectus a dolor interdum facilisis. Ut a malesuada metus, a feugiat sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce id nibh ligula. Fusce elementum elit eros, vel malesuada quam interdum sed. Suspendisse varius placerat enim non tempor.",
	};

	const [classInfo, setClassInfo] = useState(initialClassState);

	// {props.match.params.id} Get the number after /classes/

	useEffect(() => {
		// When the form first loads, the scroll position from the previous screen is maintained. This puts it to the top.
		window.scrollTo(0, 0);
		document.title = "Booking";
	}, []);

	return (
		<>
			<Container>
				<h2 style={{ textAlign: "center" }}>
					{" "}
					Booking {classInfo.time} to{" "}
					{moment(classInfo.time, "HH:mm")
						.add(parseFloat(classInfo.durationInHours), "hours")
						.format("HH:mm")}{" "}
					{classInfo.date}
				</h2>
				<Formik
					initialValues={{
						firstName: "",
						lastName: "",
						email: "",
						phoneNumber: "",
						addressLine1: "",
						addressLine2: "",
						city: "",
						county: "",
						postCode: "",
						bakingExperience: "",
						numberOfPeople: "1",
					}}
					validationSchema={Yup.object({
						firstName: Yup.string()
							.required("Required")
							.min(2, "Must be at least two characters"),
						lastName: Yup.string()
							.required("Required")
							.min(2, "Must be at least two characters"),
						email: Yup.string()
							.email("Invalid email address")
							.required("Required"),
						phoneNumber: Yup.string().max(
							11,
							"Phone numbers must be less than 14 characters"
						),
						addressLine1: Yup.string()
							.min(5, "Must be at least five characters")
							.required("Required"),
						addressLine2: Yup.string().min(
							5,
							"Must be at least five characters"
						),
						city: Yup.string()
							.required("Required")
							.min(3, "Must be at least three characters"),
						county: Yup.string()
							.min(4, "Must be at least four characters")
							.required("Required"),
						postCode: Yup.string()
							.required("Required")
							.matches(
								"^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([AZa-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z]))))[0-9][A-Za-z]{2})$",
								"Must be a valid UK postcode"
							),
						bakingExperience: Yup.string().max(
							500,
							"Must be less than 500 characters"
						),
						numberOfPeople: Yup.string()
							.min(
								1,
								"Must have at least one person! And if you're seeing this message... let us know, because you shouldn't be!"
							)
							.required("Required"),
						// .required('Required')
						// .matches('^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$ ', 'Phone number is invalid')
					})}
					onSubmit={(values, { setSubmitting }) =>
						setTimeout(() => {
							alert(JSON.stringify(values, null, 2));
							setSubmitting(false);
						}, 400)
					}
				>
					{({ handleSubmit, getFieldProps, values }) => (
						<Form onSubmit={handleSubmit}>
							<Form.Row>
								<Form.Group as={Col} controlId="firstName" className="required">
									<TextInputField
										label="First Name"
										name="firstName"
										placeholder="Joe"
									/>
								</Form.Group>
								<Form.Group as={Col} controlId="lastName" className="required">
									<TextInputField
										label="Last Name"
										name="lastName"
										placeholder="Bloggs"
									/>
								</Form.Group>
							</Form.Row>

							<Form.Row>
								<Form.Group as={Col} controlId="email" className="required">
									<TextInputField
										label="Email"
										name="email"
										placeholder="joe.bloggs@emailprovider.com"
									/>
								</Form.Group>
								<Form.Group as={Col} controlId="phoneNumber">
									<TextInputField
										label="Phone Number"
										name="phoneNumber"
										placeholder="07123456789"
									/>
								</Form.Group>
							</Form.Row>

							<Form.Row>
								<Form.Group
									as={Col}
									controlId="addressLine1"
									className="required"
								>
									<TextInputField
										label="Address"
										name="addressLine1"
										placeholder="23 Bobsroad"
									/>
								</Form.Group>
							</Form.Row>

							<Form.Row>
								<Form.Group as={Col} controlId="addressLine2">
									<TextInputField
										name="addressLine2"
										placeholder="Bobsfield Estate"
									/>
								</Form.Group>
							</Form.Row>

							<Form.Row>
								<Form.Group as={Col} controlId="city" className="required">
									<TextInputField
										label="City"
										name="city"
										placeholder="Cambridge"
									/>
								</Form.Group>
								<Form.Group as={Col} controlId="county" className="required">
									<TextInputField
										label="County"
										name="county"
										placeholder="Cambridgeshire"
									/>
								</Form.Group>
								<Form.Group as={Col} controlId="postCode" className="required">
									<TextInputField
										label="Postcode"
										name="postCode"
										placeholder="CB99 9AA"
									/>
								</Form.Group>
							</Form.Row>

							<Form.Row>
								<TextInputField
									label="Tell us about any previous baking experience"
									name="bakingExperience"
									placeholder="I've never made sourdough before. I need help with the starter. etc"
									as="textarea"
								/>
							</Form.Row>
							<Form.Row>
								<Form.Group
									as={Col}
									controlId="numberOfPeople"
									className="required"
								>
									<Form.Label htmlFor="numberOfPeople">
										Number of People:
									</Form.Label>
									{values.numberOfPeople}
									{values.numberOfPeople < 2 ? " person" : " people"}
									<Form.Control
										type="range"
										min="1"
										max={classInfo.spacesAvailable}
										{...getFieldProps("numberOfPeople")}
										style={{ borderRadius: "5px" }}
									/>
								</Form.Group>
							</Form.Row>

							<Button
								className="btn btn-block"
								type="submit"
								style={{ marginTop: "2em", marginBottom: "2em" }}
							>
								Done
							</Button>
						</Form>
					)}
				</Formik>
			</Container>
		</>
	);
};

export default ClassBookingForm;
