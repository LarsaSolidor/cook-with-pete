import React from "react";
import { Navbar, NavbarBrand, Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

function NavbarContainer() {
	return (
		<Navbar bg="light" fixed="top" sticky="top" expand="md">
			<NavbarBrand>
				<img
					src="https://cdn.onlinewebfonts.com/svg/img_478096.png"
					style={{ marginRight: "1em", marginLeft: "0.45em" }}
					width="25"
					height="25"
					alt="logo"
				/>
				Breaking Bread
			</NavbarBrand>
			<Navbar.Toggle />
			<Navbar.Collapse id="navbar-nav" className="justify-content-end">
				<Nav variant="pills">
					<LinkContainer to="/" exact={true}>
						<Nav.Link eventKey="home">Home</Nav.Link>
					</LinkContainer>
					<LinkContainer to="/classes">
						<Nav.Link eventKey="classes">Classes</Nav.Link>
					</LinkContainer>
					<Nav.Link eventKey="check-booking" href="/booking/" disabled>
						Check Booking
					</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}

export default NavbarContainer;
