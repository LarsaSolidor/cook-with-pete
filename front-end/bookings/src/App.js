import React, { Component } from "react";
import "./App.scss";
import { Route, Switch } from "react-router-dom";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import Classes from "./components/Classes";
import ClassBookingForm from "./components/ClassBookingForm";

class App extends Component {
  componentDidMount() {
    document.title = "Breaking Bread";
  }
  render() {
    return (
      <>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/classes" component={Classes} />
          <Route path="/classes/:id" component={ClassBookingForm} />
        </Switch>
      </>
    );
  }
}
export default App;
