from fastapi import FastAPI
from db import init_db
from models import Customers, Customers_Pydantic, CustomersIn_Pydantic, Events, Events_Pydantic, EventsIn_Pydantic, Locations, Locations_Pydantic, LocationsIn_Pydantic
from typing import List

app = FastAPI()


@app.on_event("startup")
async def startup_event():
    print('Starting up...')
    init_db(app)


@app.get('/customers', response_model=List[Customers_Pydantic])
async def get_all_customers():
    return await Customers_Pydantic.from_queryset(Customers.all())


@app.get('/customers/{customer_id}', response_model=Customers_Pydantic)
async def get_customer(customer_id: int):
    return await Customers_Pydantic.from_queryset_single(Customers.get(id=customer_id))


@app.post('/customers', response_model=Customers_Pydantic)
async def create_customer(customer: CustomersIn_Pydantic):
    customer_obj = await Customers.create(**customer.dict(exclude_unset=True))
    return await Customers_Pydantic.from_tortoise_orm(customer_obj)


@app.delete('/customers/{customer_id}', response_model=dict)
async def delete_customer(customer_id: int):
    await Customers.filter(id=customer_id).delete()
    return {}


@app.get('/events', response_model=List[Events_Pydantic])
async def get_all_events():
    return await Events_Pydantic.from_queryset(Events.all())


@app.post('/events', response_model=Events_Pydantic)
async def create_event(event: EventsIn_Pydantic):
    event_obj = await Events.create(**event.dict(exclude_unset=True))
    return await Events_Pydantic.from_tortoise_orm(event_obj)


@app.get('/locations', response_model=List[Locations_Pydantic])
async def get_all_locations():
    return await Locations_Pydantic.from_queryset(Locations.all())


@app.post('/locations', response_model=Locations_Pydantic)
async def create_location(location: LocationsIn_Pydantic):
    location_obj = await Locations.create(**location.dict(exclude_unset=True))
    return await Locations_Pydantic.from_tortoise_orm(location_obj)
