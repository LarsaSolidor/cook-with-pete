from os import environ
from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

mysql_user = environ.get('BOOKINGS_MYSQL_USER') or 'directus'
mysql_password = environ.get('BOOKINGS_MYSQL_PASSWORD') or 'F3W!l95^A^wCJApCH7@X'
mysql_host = environ.get('BOOKINGS_MYSQL_HOST') or '192.168.0.254'
mysql_port = environ.get('BOOKINGS_MYSQL_PORT') or '3306'
mysql_db = environ.get('BOOKINGS_MYSQL_DB') or 'directus'

db_url = f"mysql://{mysql_user}:{mysql_password}@{mysql_host}:{mysql_port}/{mysql_db}"

TORTOISE_ORM = {
    "connections": {"default": db_url},
    "apps": {
        "models": {
            "models": ['models', 'aerich.models'],
            "default_connection": "default",
        }
    },
}


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        db_url=f"mysql://{mysql_user}:{mysql_password}@{mysql_host}:{mysql_port}/{mysql_db}",
        modules={'models': ['models']},  # a list of the python filenames where models are stored
        generate_schemas=False,
        add_exception_handlers=True,
    )
