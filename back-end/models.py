from tortoise import fields, models
from tortoise.models import Model
from tortoise.contrib.pydantic import pydantic_model_creator
from enum import Enum
from tortoise.validators import Validator
from tortoise.exceptions import ValidationError


class PublishedStatus(str, Enum):
    draft = 'draft'
    published = 'published'
    deleted = 'deleted'


class Customers(Model):
    id = fields.IntField(pk=True)
    created_on = fields.DatetimeField(auto_now_add=True, description='When first added')
    updated_on = fields.DatetimeField(auto_now=True, description='When last updated')
    first_name = fields.CharField(255)
    last_name = fields.CharField(255)
    email_address = fields.CharField(255, unique=True)
    password = fields.CharField(255, description='Hashed password')
    mobile = fields.CharField(20)

    events = fields.ReverseRelation["CustomerEvent"]

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.email_address}"

    class Meta:
        table = 'customers'
        table_description = 'List of customer accounts'

    class PydanticMeta:
        exclude = ["password"]


class Locations(Model):
    id = fields.IntField(pk=True)
    created_on = fields.DatetimeField(auto_now_add=True, description='When first added')
    updated_on = fields.DatetimeField(auto_now=True, description='When last updated')
    status: PublishedStatus = fields.CharEnumField(PublishedStatus, default=PublishedStatus.draft)
    name = fields.CharField(255, null=False, description='The name of the venue or location')
    address_1 = fields.CharField(255, null=False)
    address_2 = fields.CharField(255)
    city = fields.CharField(255, null=False)
    postcode = fields.CharField(255, null=False)
    description = fields.CharField(500, description='A lovely description of the location')

    events: fields.ReverseRelation["Events"]

    def __str__(self):
        return f"{self.name} ({self.postcode})"

    class Meta:
        table = 'locations'
        table_description = 'List of locations that events can be held at'


class Events(Model):
    id = fields.IntField(pk=True)
    created_on = fields.DatetimeField(auto_now_add=True, description='When first added')
    updated_on = fields.DatetimeField(auto_now=True, description='When last updated')
    status: PublishedStatus = fields.CharEnumField(PublishedStatus, default=PublishedStatus.draft)
    start = fields.DatetimeField(null=False)
    end = fields.DatetimeField(null=False)
    spaces = fields.IntField(null=False, description='The number of spaces available for the event')
    base_price = fields.IntField(null=False, description='The normal price for one person in pence')
    description = fields.CharField(500, description='Event specific details')
    location: fields.ForeignKeyRelation[Locations] = fields.ForeignKeyField(
        'models.Locations', related_name='events'
    )

    customers: fields.ReverseRelation["CustomerEvent"]

    def __str__(self):
        return f"{self.start}"

    class Meta:
        table = 'events'
        table_description = 'List of events'

    class PydanticMeta:
        spaces: conint(ge=1)


class CustomerEvent(Model):
    id = fields.IntField(pk=True)
    created_on = fields.DatetimeField(auto_now_add=True, description='When first added')
    updated_on = fields.DatetimeField(auto_now=True, description='When last updated')
    # customer_id = fields.ForeignKeyField('models.Customers', related_name='events')
    customer: fields.ForeignKeyRelation[Customers] = fields.ForeignKeyField(
        "models.Customers", related_name='customer_event'
    )
    # event_id = fields.ForeignKeyField('models.Events', related_name='customers')
    event: fields.ForeignKeyRelation[Events] = fields.ForeignKeyField(
        "models.Events", related_name='customer_event'
    )
    attended = fields.BooleanField(description='Whether the customer attended the event or not')

    def __str__(self):
        return f"{self.created_on} Event:{self.event_id} Customer:{self.customer_id} Attended:{self.attended}"

    class Meta:
        table = 'customer_event'
        table_description = 'Who has purchased which event and whether they attended'





Customers_Pydantic = pydantic_model_creator(Customers, name='Customers')
CustomersIn_Pydantic = pydantic_model_creator(Customers, name='CustomersIn', exclude_readonly=True)

Locations_Pydantic = pydantic_model_creator(Locations, name='Locations')
LocationsIn_Pydantic = pydantic_model_creator(Locations, name='LocationsIn', exclude_readonly=True)

Events_Pydantic = pydantic_model_creator(Events, name='Events')
print(Events_Pydantic.schema_json(indent=2))
EventsIn_Pydantic = pydantic_model_creator(Events, name='EventsIn', exclude_readonly=True)

CustomerEvent_Pydantic = pydantic_model_creator(CustomerEvent, name='CustomerEvent')
CustomerEventIn_Pydantic = pydantic_model_creator(CustomerEvent, name='CustomerEventIn', exclude_readonly=True)